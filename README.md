# Custom disclaimer

This is a modified disclaimer module set.


## How to use it

1. Copy this over the `sites/all/` directory
2. Enable all provided modules
3. Setup Disclaimer module to not show the disclaimer by default
4. Go to *Structure* > *Custom Publishing Options*
5. Create a publishing option *Show disclaimer* with a machine name
   `disclaimer_enabled`
6. Go to a node edit page
7. Open *Publishing options* tab and tick *Show disclaimer* option you've
   created earlier.
8. Profit!
